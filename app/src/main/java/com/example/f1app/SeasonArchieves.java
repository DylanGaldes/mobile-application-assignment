package com.example.f1app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class SeasonArchieves extends AppCompatActivity {

    Spinner spin;
    TextView archieveRes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season_archieves);
        spin = findViewById(R.id.spinner);
        appendSpinner();
        archieveRes = findViewById(R.id.archieveResults);
    }
    public void appendSpinner(){
        ArrayList<String> list = new ArrayList<>();
        for (int i = 1960; i<=2018;i++){
            list.add(Integer.toString(i));
        }
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
        spin.setAdapter(ad);
    }

    public String createUrl(){
        String first = "https://ergast.com/api/f1/";
        String last =  "/constructorStandings.json";
        ;
        return first+spin.getSelectedItem().toString()+last;
    }


    public void loadArchieveData(View view) {
        String url = createUrl();
        FetchArchieve faa = new FetchArchieve(url, archieveRes);
        faa.execute();
    }
}
