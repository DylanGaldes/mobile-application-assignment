package com.example.f1app;

import android.os.AsyncTask;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FetchArchieve extends AsyncTask<Void,Void,String> {
    String urll;
    TextView txContent;

    public FetchArchieve(String url, TextView cont) {

        this.txContent = cont;
        this.urll = url;
    }


    @Override
    protected String doInBackground(Void... voids) {
        try{
            URL url = new URL(urll);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            if(inputStream == null){
                return"Data is not fetched";
            }
            else{
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuilder stringBuilder = new StringBuilder();

                while((line=br.readLine())!=null){
                    stringBuilder.append(line);

                }
                return stringBuilder.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        String sl = "";

        if(s.equalsIgnoreCase("Data is not fetched")){
            txContent.setText("Data is not fetched for some Reason");
        }
        else{
            try {

                //Continue From Here
                JSONObject root = new JSONObject(s);
                JSONObject mrData = root.getJSONObject("MRData");
                JSONObject arr = mrData.getJSONObject("StandingsTable");
                JSONArray standings = arr.getJSONArray("StandingsLists");
                JSONObject arrayItem = standings.getJSONObject(0);
                JSONArray arrayStandings = arrayItem.getJSONArray("ConstructorStandings");

                for (int i = 0; i < arrayStandings.length(); i++) {
                    JSONObject jsonObject = arrayStandings.getJSONObject(i);
                    JSONObject constructor = jsonObject.getJSONObject("Constructor");
                    sl += "Position: "+ jsonObject.getString("position")+" Team: "+constructor.getString("name")+" Points: "+jsonObject.getString("points")+"\n" ;
                }

                txContent.setTextSize(15);
                txContent.setText(sl);
                txContent.setMovementMethod(new ScrollingMovementMethod());
                txContent.setText(sl);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}

