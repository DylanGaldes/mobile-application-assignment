package com.example.f1app;

import android.os.AsyncTask;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class fetchDrivers extends AsyncTask<Void,Void,String> {

    public fetchDrivers( TextView cont) {
        this.txContent = cont;
    }

    TextView txContent;
    @Override
    protected String doInBackground(Void... voids) {
        try{
            URL url = new URL("https://ergast.com/api/f1/2010/drivers.json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            if(inputStream == null){
                return"Data is not fetched";
            }
            else{
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuilder stringBuilder = new StringBuilder();
                while((line=br.readLine())!=null){
                    stringBuilder.append(line);
                }
                return stringBuilder.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        String sl = "";

        if(s.equalsIgnoreCase("Data is not fetched")){
            txContent.setText("Data is not fetched for some Reason");
        }
        else{
            try {

                JSONObject root = new JSONObject(s);
                JSONObject mrData = root.getJSONObject("MRData");
                JSONObject arr = mrData.getJSONObject("DriverTable");
                JSONArray drivers = arr.getJSONArray("Drivers");


                for (int i = 0; i < drivers.length(); i++) {
                    JSONObject driver = drivers.getJSONObject(i);

                    sl +=" Driver: "+driver.getString("givenName")+" "+driver.getString("familyName")+" DOB: "+driver.getString("dateOfBirth")+" Nationality: "+driver.getString("nationality")+"\n";
                }

                txContent.setClickable(true);
                txContent.setMovementMethod(LinkMovementMethod.getInstance());
                txContent.setText(sl);
                txContent.setMovementMethod(new ScrollingMovementMethod());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}