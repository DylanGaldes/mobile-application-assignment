package com.example.f1app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class thisSeason extends AppCompatActivity {

    //Button btnD, btnC;
    TextView driverTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_this_season);
        driverTxt = findViewById(R.id.txtStandings);
    }

    public void loadDriverData(View view) {

        FetchAnActivity faa = new FetchAnActivity(driverTxt, "https://ergast.com/api/f1/2019/driverStandings.json");

        faa.execute();
    }
    public void loadConstructorData(View view){
        FetchAnActivity faa = new FetchAnActivity(driverTxt, "https://ergast.com/api/f1/2019/constructorStandings.json");

        faa.execute();
    }

}
