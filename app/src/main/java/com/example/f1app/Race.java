package com.example.f1app;

import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Race extends AppCompatActivity {
     TextView txTitle, txContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_race);
        txTitle = findViewById(R.id.txtGrandPrix);
        txContent = findViewById(R.id.txtStand);

        FetchRace faa = new FetchRace(txTitle, txContent);
        faa.execute();
    }
}

