package com.example.f1app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Driver extends AppCompatActivity {
    TextView txContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        txContent = findViewById(R.id.txtDrivers);

        fetchDrivers faa = new fetchDrivers(txContent);
        faa.execute();
    }
}
