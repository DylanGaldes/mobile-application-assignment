package com.example.f1app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class applicationMainMenu extends AppCompatActivity {

    Button btn1, btn2, btn3, btn4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_main_menu);
        btn1 =  findViewById(R.id.btnSeason);
        btn2 =  findViewById(R.id.btnDriver);
        btn3 =  findViewById(R.id.btnSpecSeason);
        btn4 =  findViewById(R.id.btnRace);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(applicationMainMenu.this, thisSeason.class));
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //Go to Drivers
                startActivity(new Intent(applicationMainMenu.this, Driver.class));
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(applicationMainMenu.this, SeasonArchieves.class));
            }

        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //Go to Race results
                startActivity(new Intent(applicationMainMenu.this, Race.class));
            }
        });
    }

}

