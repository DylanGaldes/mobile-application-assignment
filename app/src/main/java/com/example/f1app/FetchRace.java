package com.example.f1app;

import android.os.AsyncTask;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FetchRace extends AsyncTask<Void,Void,String> {

    public FetchRace(TextView Title, TextView cont) {
        this.txTitle = Title;
        this.txContent = cont;
    }

    TextView txTitle, txContent;
    @Override
    protected String doInBackground(Void... voids) {
        try{
            URL url = new URL("https://ergast.com/api/f1/current/last/results.json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            if(inputStream == null){
                return"Data is not fetched";
            }
            else{
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuilder stringBuilder = new StringBuilder();
                while((line=br.readLine())!=null){
                    stringBuilder.append(line);
                }
                return stringBuilder.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        String sl = "";

        if(s.equalsIgnoreCase("Data is not fetched")){
            txTitle.setText("Data is not fetched for some Reason");
        }
        else{
            try {

                JSONObject root = new JSONObject(s);
                JSONObject mrData = root.getJSONObject("MRData");
                JSONObject arr = mrData.getJSONObject("RaceTable");
                JSONArray races = arr.getJSONArray("Races");
                JSONObject arrayItem = races.getJSONObject(0);
                JSONArray arrayStandings = arrayItem.getJSONArray("Results");
                txTitle.setText("           "+arrayItem.getString("raceName"));

                for (int i = 0; i < arrayStandings.length(); i++) {
                    JSONObject jsonObject = arrayStandings.getJSONObject(i);
                    JSONObject driver = jsonObject.getJSONObject("Driver");

                    String totalTime ="";
                    try{
                        JSONObject time = jsonObject.getJSONObject("Time");
                         totalTime = time.getString("time");
                    }catch(Exception e){
                        totalTime = "N/A";
                    }

                    sl += "     Position: "+ jsonObject.getString("position")+" Driver: "+driver.getString("givenName")+" "+driver.getString("familyName")+" Status: "+jsonObject.getString("status")+"\n" +
                            "" +
                            "" +
                            "     Time: "+totalTime+"\n" ;
                }

               // txContent.setTextSize(15);
                txContent.setText(sl);
                txContent.setMovementMethod(new ScrollingMovementMethod());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
